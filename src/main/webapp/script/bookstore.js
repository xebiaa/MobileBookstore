function getproductlist() {
    $.getJSON("rest/catalog/1/getproductlist",
            function(data) {
                if (data != null) {
                    $.each(data.product, function(i, item) {
                        thisItem = $("<li class='arrow' onclick='getproduct(\""+item.id+ "\")'/>");
                        thisItem.appendTo("#booklist");
                        $("<a/>").attr("href", "#book").text(item.title).appendTo(thisItem);
                    });
                }
            });
}

function getproduct(id) {
    $.getJSON("rest/catalog/1/getproduct/" + id,
            function(data) {
                if (data != null) {
                     $("#booktitle").text(data.title);
                     $("#bookimg").attr("src", data.url);
                }
            });
}