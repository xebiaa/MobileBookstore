package com.xebia.mobile.bookstore.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/** @author mischa */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductType")
@XmlRootElement(name = "product")
public class Product {
    private String id;
    private String title;
    private String description;
    private String price;
    private String url;

    public Product() {
    }

    public Product(String id, String description, String price, String title, String url) {
        this.id = id;
        this.description = description;
        this.price = price;
        this.title = title;
        this.url = url;
    }

    public String getId() {
        return id;
    }
}
