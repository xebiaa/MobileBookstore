package com.xebia.mobile.bookstore.rest;

import com.xebia.mobile.bookstore.domain.Product;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: dbxmda
 * Date: 22-nov-2010
 * Time: 11:28:40
 * To change this template use File | Settings | File Templates.
 */
@Path("/catalog/1/")
public class Catalog {
    private List<Product> products;

    public Catalog() {
        products = new ArrayList<Product>();
        products.add(new Product("1", "De aanslag van Harry Mulisch", "30,00", "De aanslag", "http://static.bol.com/imgbase0/imagebase/regular/FC/8/1/1/9/1001004010939118.jpg"));
        products.add(new Product("2", "De ontdekking van de hemel van Harry Mulisch", "32,00", "De ontdekking van de hemel", "http://static.bol.com/imgbase0/imagebase/regular/FC/3/2/1/9/1001004010939123.jpg"));

    }

    @Path("getproductlist")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Product> getProductList() {
        return products;
    }

    @Path("getproduct/{id}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Product getProduct(@PathParam("id") String id) {
        for (Product product : products) {
            if (product.getId().equals(id)) {
                return product;
            }
        }
        return new Product();
    }
}
